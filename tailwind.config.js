/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {
      fontFamily: {
        'red-hat-display': ['Red Hat Display', 'sans-serif']
      },
      colors: {
        'pale-blue': 'hsl(225, 100%, 94%)',
        'very-pale-blue': 'hsl(225, 100%, 98%)',
        'desaturated-blue': 'hsl(224, 23%, 55%)',
        'bright-blue': 'hsl(245, 75%, 52%)',
        'dark-blue': 'hsl(223, 47%, 23%)'
      },
      backgroundImage: {
        'desktop-illustration': 'url("./images/pattern-background-desktop.svg")',
        'mobile-illustration': 'url("./images/pattern-background-mobile.svg")'
      }
    },
  },
  plugins: [],
}
